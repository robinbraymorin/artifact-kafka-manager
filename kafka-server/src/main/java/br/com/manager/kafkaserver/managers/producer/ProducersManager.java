package br.com.manager.kafkaserver.managers.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class ProducersManager {

    /**
     *
     * @param propertiesProducer Properties for producer what will create
     * @return Producer<String,String>
     */
    public static Producer<String, String> createProducer(Properties propertiesProducer){
        return new KafkaProducer<>(propertiesProducer, new StringSerializer(), new StringSerializer());
    }

    public static void sendMessageProducer(Producer<String, String> producer, String key, String message ,String nameTopic) throws InterruptedException {
        try {
            final ProducerRecord<String, String> record = new ProducerRecord<>(nameTopic, key , message);
            producer.send(record, (metadata, exception) -> {
                    if (metadata != null) {
                        System.out.printf("Sent it record (key=%s value=%s)", record.key(), record.value());
                    } else {
                        exception.printStackTrace();
                    }
            });
        }finally {
            producer.flush();
        }
    }

}
