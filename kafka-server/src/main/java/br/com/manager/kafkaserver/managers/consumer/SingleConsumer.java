package br.com.manager.kafkaserver.managers.consumer;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class SingleConsumer {

    public static Consumer<String, String> createConsumer(List<String> topics, Properties props) {
        final Consumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(topics.size()== 1 ? Collections.singletonList(topics.get(0)) : topics);
        return consumer;
    }

    public static void readMessageConsumer(Consumer<String,String> consumer, int readUntil) throws InterruptedException {
        int noRecordsCount = 0;
        while (true) {
            final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(1000));
            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > readUntil) break;
                else continue;
            }
            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%d, %s, %d, %d)\n", record.key(), record.value(), record.partition(), record.offset());
            });
            consumer.commitAsync();
        }
        consumer.close();
    }

}
