package br.com.manager.kafkaserver.managers.topic;

import br.com.manager.properties.PropertiesReader;
import kafka.admin.RackAwareMode;
import kafka.server.KafkaServer;
import kafka.zk.AdminZkClient;
import scala.collection.JavaConverters;

import java.util.List;
import java.util.Properties;

public class TopicsManager {

    /**
     * Create a New Topic
     * @param nameTopic Name of Topic what will create
     * @param kafkaServer Kafka Server on topic will be create
     * @return boolean
     */
    public static boolean createTopic(String nameTopic, KafkaServer kafkaServer){
        if(!kafkaServer.kafkaController().isActive()) return false;
        if(!kafkaServer.zkClient().topicExists(nameTopic)){
            Properties topicsProperties = PropertiesReader.getProps("./topics.properties");
            AdminZkClient adminZkClient = new AdminZkClient(kafkaServer.zkClient());
            adminZkClient.createTopic(nameTopic, Integer.valueOf(topicsProperties.getProperty("num.partitions")),
                    Integer.valueOf(topicsProperties.getProperty("num.replication")), topicsProperties, RackAwareMode.Safe$.MODULE$);
        }
        return kafkaServer.zkClient().topicExists(nameTopic);
    }

    /**
     * List all TopicsManager of Kafka Server
     * @param kafkaServer Kafka Server on will all topics will be consulting
     * @return List of topics on this Kafka Server
     */
    public static List<String> listAllTopicsInServer(KafkaServer kafkaServer){
        return JavaConverters.seqAsJavaList(kafkaServer.zkClient().getAllTopicsInCluster());
    }

    /**
     * Delete a topics that exists in kafka server
     * @param nameTopic Name of Topics what will be delete
     * @param kafkaServer Kafka Server on the topic will is rolling
     * @return boolean
     */
    public static boolean deleteTopic(String nameTopic, KafkaServer kafkaServer) {
        new AdminZkClient(kafkaServer.zkClient()).deleteTopic(nameTopic);
        return kafkaServer.zkClient().topicExists(nameTopic);
    }
}

