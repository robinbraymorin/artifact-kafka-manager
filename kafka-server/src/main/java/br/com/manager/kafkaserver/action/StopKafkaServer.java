package br.com.manager.kafkaserver.action;

public class StopKafkaServer {
    public static void stop(){
        try {
            if (StartKafkaServer.server.kafkaController().isActive()) StartKafkaServer.server.shutdown();
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
