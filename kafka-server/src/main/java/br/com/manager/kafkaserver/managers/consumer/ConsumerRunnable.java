package br.com.manager.kafkaserver.managers.consumer;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class ConsumerRunnable implements Runnable {
    private KafkaConsumer<String, String> consumer;
    private List<String> topics;
    private int id;
    Properties props;

    public ConsumerRunnable(int id, List<String> topics,Properties props) {
        this.id = id;
        this.topics = topics;
        this.consumer = new KafkaConsumer<>(props);
    }

    @Override
    public void run() {
        try {
            consumer.subscribe(topics);
            while (true) {
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(Long.MAX_VALUE));
                for (ConsumerRecord<String, String> record : records) {
                    Map<String, Object> data = new HashMap<>();
                    data.put("partition", record.partition());
                    data.put("offset", record.offset());
                    data.put("value", record.value());
                    System.out.println(this.id + ": " + data);
                }
            }
        } catch (WakeupException e) {
            // used for shutdown
        } finally {
            consumer.close();
        }
    }

    public void shutdown() {
        //Active exception -> WakeupException
        consumer.wakeup();
    }
}
