package br.com.manager.kafkaserver.action;

import br.com.manager.properties.PropertiesReader;
import br.com.manager.zookeeperserver.action.StartZKServer;
import io.debezium.annotation.ThreadSafe;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServer;
import org.apache.kafka.common.utils.Time;
import scala.Option;
import scala.collection.mutable.ArraySeq;

import java.util.Properties;
import java.util.function.Supplier;

@ThreadSafe
public class StartKafkaServer implements Runnable{

    private Supplier<String> zkConnection;
    private final static Properties config = PropertiesReader.getProps("./server.properties");
    public static volatile KafkaServer server;

    public static void main(String[] args){
        StartKafkaServer startKafkaServer = new StartKafkaServer();
        startKafkaServer.execute();
    }

    public void execute() {
        if (server != null) {
            throw new IllegalStateException("" + server + " is already running");
        }
        // Start the server ...
        new Thread(this::run).start();
    }

    public void run() {
        try {
//            if(StartZKServer.threadServer == null)
//            StartZKServer.threadServer.start();
            StartZKServer zkServer = new StartZKServer();
            zkServer.run();
            server = new KafkaServer(new KafkaConfig(config), Time.SYSTEM, Option.apply(null),
                    new ArraySeq<>(0));
            server.startup();
        } catch (RuntimeException e) {
            server = null;
            throw e;
        }
    }
}
