package br.com.manager.properties;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {

    private static Properties props = null;

    private static void load(String filePath){
        try {
            FileInputStream file = new FileInputStream(filePath);
            props = new Properties();
            props.load(file);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static Properties getProps(String filePath){
        load(filePath);
        return props;
    }
}
