package br.com.manager.zookeeperserver.action;

public class StopZKServer {

   public static void shutdown() {

       if (StartZKServer.threadServer != null) {
          try {
             StartZKServer.threadServer.join();
          } catch (InterruptedException e) {
             e.printStackTrace();
          }
       }
   }
}
