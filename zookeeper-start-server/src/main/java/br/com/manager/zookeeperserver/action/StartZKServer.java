package br.com.manager.zookeeperserver.action;

import br.com.manager.properties.PropertiesReader;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.admin.AdminServer;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;

import javax.annotation.concurrent.ThreadSafe;
import java.io.IOException;
import java.util.Properties;

@ThreadSafe
public class StartZKServer implements Runnable {

   public static Thread threadServer = null;
   public static final ZooKeeperServerMain zooKeeperServer = new ZooKeeperServerMain();

   public static void main(String args[]){
       StartZKServer startZKServer = new StartZKServer();
       startZKServer.run();
   }

   public void run(){
       Properties startupProperties = PropertiesReader.getProps("./zookeeperserver.properties");

       QuorumPeerConfig quorumConfiguration = new QuorumPeerConfig();
       try {
           quorumConfiguration.parseProperties(startupProperties);
       } catch(Exception e) {
           throw new RuntimeException(e);
       }

       final ServerConfig configuration = new ServerConfig();
       configuration.readFrom(quorumConfiguration);

       threadServer = new Thread(() -> {
           try {
               zooKeeperServer.runFromConfig(configuration);
           } catch (IOException e) {
               System.out.println("Loading ZooKeeper Server Failed");
           } catch (AdminServer.AdminServerException e) {
               e.printStackTrace();
           }
       });
       threadServer.start();
   }

}
